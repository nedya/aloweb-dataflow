import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import datetime

def debug_function(pcollection_as_list):
    print (pcollection_as_list)

# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.137.27/alodokter"
connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.10/alodokter"

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'

   #  pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
   #     "--project", "plenary-justice-151004",
   #     "--staging_location", ("%s/staging/" %gcs_path),
   #     "--temp_location", ("%s/temp" % gcs_path),
   #     "--region", "asia-east1",
   #     "--setup_file", "./setup.py"
   # ])
# def run():
#     options = PipelineOptions()
#     google_cloud_options = options.view_as(GoogleCloudOptions)
#     google_cloud_options.project = 'plenary-justice-151004'
#     google_cloud_options.job_name = 'sourcemongo'
#     google_cloud_options.staging_location = 'gs://staging-plenary-justice-151004/staging/'
#     google_cloud_options.temp_location = 'gs://staging-plenary-justice-151004/temp'
#     google_cloud_options.region = 'asia-east1'
#     options.view_as(StandardOptions).runner = 'DirectRunner'
#     options.view_as(SetupOptions).save_main_session = True
#
#     pipeline = beam.Pipeline(options=options)

    with beam.Pipeline(options = options) as pipeline_question_per_day:
# with beam.Pipeline(runner='DirectRunner') as pipeline_question_per_day:
        (pipeline_question_per_day
             | 'Readquestion_per_dayWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_posts',
                                      query={'post_date':{'$gte':datetime.datetime(2018,11,14)}}, fields=['post_date'])
             | 'Format' >> beam.Map(lambda x:(str(x['post_date'])[0:10], x['_id'] ))
             | 'Count' >> beam.GroupByKey()
             | 'len' >> beam.Map(lambda (x,y):{'post_date':x, 'number_of_questions':len(list(y))})
            # |'Debugy' >> beam.Map(debug_function)
             | 'WritePageViewToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','question_per_day'),
                     schema='post_date:DATETIME, number_of_questions:INTEGER',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
             )
