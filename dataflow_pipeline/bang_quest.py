import apache_beam as beam
import datetime
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import uuid, math

def debug_function(pcollection_as_list):
    print (pcollection_as_list)
# connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.10/alodokter"
#connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.137.27/alodokter"
# connection_string = "mongodb://aloadmin:mutia.1975.dokter@35.187.240.157/admin"
connection_string = "mongodb://127.0.0.1:27017/pobpad"

tdelta = datetime.timedelta(hours=7)
#start = datetime.datetime(2018, 8, 1)

def slugtopic(topic):
    if topic['post_date'] is None:
        post_date = None
    else:
        post_date = str(topic['post_date']+tdelta)[0:19]

    if topic['pickup_date'] is None:
        pickup_date = None
    else:
        pickup_date = str(topic['pickup_date']+tdelta)[0:19]

    if topic['core_post_meta'] is not None:
        for i in topic['core_post_meta']:
            if "custom_permalink" in i["meta_key"]:
                return {'post_id':topic['post_id'],
                        'post_title':topic['post_title'],
                        'post_status':topic['post_status'],
                        'post_date':post_date,
                        'pickup_date':pickup_date,
                        'slug':i['meta_value']}
            elif "custom_permalink" not in i["meta_key"]:
                return {'post_id':topic['post_id'],
                        'post_title':topic['post_title'],
                        'post_status':topic['post_status'],
                        'post_date':post_date,
                        'pickup_date':pickup_date,
                        'slug':topic['post_name']}

    else:
        return {'post_id':topic['post_id'],
                'post_title':topic['post_title'],
                'post_status':topic['post_status'],
                'post_date':post_date,
                'pickup_date':pickup_date,
                'slug':topic['post_name']}

def run():
   #  gcs_path = "gs://staging-plenary-justice-151004"
   #  dataflow_options = [
   #      "--project", "plenary-justice-151004",
   #      "--staging_location", ("%s/staging/" %gcs_path),
   #      "--temp_location", ("%s/temp" % gcs_path),
   #      "--region", "asia-east1",
   #      "--setup_file", "./setup.py",
   #      "--num_workers", "7"
   #  ]
   #  options = PipelineOptions(dataflow_options)
   #  gcloud_options = options.view_as(GoogleCloudOptions)
   #  options.view_as(StandardOptions).runner = 'dataflow'
   # #  pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
   # #     "--project", "plenary-justice-151004",
   # #     "--staging_location", ("%s/staging/" %gcs_path),
   # #     "--temp_location", ("%s/temp" % gcs_path),
   # #     "--region", "asia-east1",
   # #     "--setup_file", "./setup.py"
   # # ])


   options = PipelineOptions()
   google_cloud_options = options.view_as(GoogleCloudOptions)
   google_cloud_options.project = 'plenary-justice-151004'
   google_cloud_options.job_name = 'sourcemongo'
   google_cloud_options.staging_location = 'gs://staging-plenary-justice-151004/staging/'
   google_cloud_options.temp_location = 'gs://staging-plenary-justice-151004/temp'
   google_cloud_options.region = 'asia-east1'
   options.view_as(StandardOptions).runner = 'DirectRunner'
   options.view_as(SetupOptions).save_main_session = False
   requirements_file = "/home/grumpycat/flowarehouse/dataflow_pipeline/requirements.txt"
   options.view_as(SetupOptions).requirements_file = requirements_file

   p = beam.Pipeline(options=options)

    # with beam.Pipeline(options = options) as pipeline_questions:
   (p
             |'ReadQuestionsWeb' >> ReadFromMongo(connection_string, 'pobpad', 'core_posts', query={'post_type':'topic'}, fields=['core_post_meta', 'post_name', 'post_id', 'post_title', 'post_status', 'post_date', 'pickup_date'])
             |'FormatQuestions' >> beam.Map(lambda x:{'core_post_meta':x.get('core_post_meta'),
                                                      'post_name':x['post_name'],
                                                      'post_id':str(x['_id']),
                                                      'post_title':x['post_title'],
                                                      'post_status':x['post_status'],
                                                      'post_date':x.get('post_date',None),
                                                      'pickup_date':x.get('pickup_date', None)})
             |'SlugTopic' >> beam.Map(slugtopic)
             # |'DebugQuestions' >> beam.Map(debug_function)
             |'WriteQuestionsToBQ' >> beam.io.Write(
               beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','pobpad_questions'),
               schema='post_id:STRING, slug:STRING, post_title:STRING, post_status:STRING, post_date:DATETIME, pickup_date:DATETIME',
               create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
               write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    )

   p.run().wait_until_finish()

if __name__ == '__main__':
    run()
