
# coding: utf-8

# In[6]:


import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import datetime

def debug_function(pcollection_as_list):
    print (pcollection_as_list)

# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.187.240.157/alodokter"
connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.10/alodokter"


# def run():
#     gcs_path = "gs://staging-plenary-justice-151004"
#     pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
#         "--project", "plenary-justice-151004",
#         "--staging_location", ("%s/staging/" %gcs_path),
#         "--temp_location", ("%s/temp" % gcs_path),
#         "--region", "asia-east1",
#         "--setup_file", "./setup.py"
#     ])

with beam.Pipeline(runner='DirectRunner') as pipeline_moderator:
    (pipeline_moderator
        |'ReadModerator' >> ReadFromMongo(connection_string, 'alodokter', 'core_users',
                                        query={'core_user_meta': {"$elemMatch": {"meta_key": "wp_capabilities", "meta_value": {"$regex": 'bbp_moderator'} } }},
                                        fields=['display_name'])
        |'FormatData' >> beam.Map (lambda x:{'id':str(x['_id']),
                                             'name':x['display_name'],
                                             'updated_at':str(datetime.datetime.now())[:19]})
        |'Debug' >> beam.Map(debug_function))
    #     | 'WriteToBQ' >> beam.io.Write(
    #          beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','moderator'),
    #          schema='id:STRING, name:STRING, updated_at:DATETIME',
    #          create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    #          write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    # )
    pipeline_moderator.run()
