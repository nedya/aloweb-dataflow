
# coding: utf-8

# In[1]:
import apache_beam as beam
import datetime
import itertools, uuid
from apache_beam.options.pipeline_options import PipelineOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
from apache_beam.io import iobase, range_trackers
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from google.oauth2.service_account import Credentials

def debug_function(pcollection_as_list):
    print (pcollection_as_list)


# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.137.27/alodokter"
connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.10/alodokter"
tdelta = datetime.timedelta(hours=7)


#fact_answered
class GetAnsweredFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alodokter
        answers = db.core_posts
        ansqueList = []
        for answer in answers.find({'post_parent_id':element['post_id']}, no_cursor_timeout=True):
            if element.get('pickup_date',None) is not None:
                pick_up_date = element.get('pick_up_date')+tdelta
                pick_up_date = str(pick_up_date)[0:19]
            elif element.get('pickup_date',None) is None:
                pick_up_date = None
                ansqueList.append({'id':str(uuid.uuid4()),
                                   'question_id':str(element.get('_id', None)),
                                   'post_id':element['post_id'],
                                   'post_modified':element.get('post_modified', None)+tdelta,
                                   'answer_id':str(answer.get('_id', None)),
                                   'doctor_id':str(answer['post_author']),
                                   'answer_time':answer['post_date']+tdelta,
                                   'user_id':str(element.get('post_author', None)),
                                   'post_date':element.get('post_date', None)+tdelta,
                                   'pick_up_date':pick_up_date,
                                   'pick_up_by_id':str(element.get('pick_up_by_id'))
                                   })

            elif element.get('pickup_date',None) is None:
                ansqueList.append({'id':str(uuid.uuid4()),
                                   'question_id':str(element.get('_id', None)),
                                   'post_id':element['post_id'],
                                   'post_modified':element.get('post_modified', None)+tdelta,
                                   'answer_id':str(answer.get('_id', None)),
                                   'doctor_id':str(answer['post_author']),
                                   'answer_time':answer['post_date']+tdelta,
                                   'user_id':str(element.get('post_author', None)),
                                   'post_date':element.get('post_date', None)+tdelta,
                                   'pick_up_date':pick_up_date,
                                   'pick_up_by_id':None
                                   })
        return (ansqueList)


def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py",
        "--num_workers", "7"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
   #  pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
   #     "--project", "plenary-justice-151004",
   #     "--staging_location", ("%s/staging/" %gcs_path),
   #     "--temp_location", ("%s/temp" % gcs_path),
   #     "--region", "asia-east1",
   #     "--setup_file", "./setup.py"
   # ])

# with beam.Pipeline(runner = 'DirectRunner') as pipeline_fact_question:
    with beam.Pipeline(options=options) as pipeline_fact_answered:
        (pipeline_fact_answered
            | 'read_factquestion' >> ReadFromMongo(connection_string, 'alodokter', 'core_posts',
                        query={'post_type':'topic', 'post_date':{'$gte':datetime.datetime(2019,1,12,7,31,38)}},
                        fields=['post_id', 'post_modified', 'post_author', 'post_date', 'pick_up_date', 'pick_up_by_id'])
            | 'read_answers' >> beam.ParDo(GetAnsweredFn())
            | 'FormatData' >> beam.Map(lambda x:{'id':x['id'],
                               'question_id':x['question_id'],
                               'post_id':str(x['post_id']),
                               'post_modified':str(x['post_modified'])[0:19],
                               'answer_id':x['answer_id'],
                               'answer_by_id':x['doctor_id'],
                               'answer_time':str(x['answer_time'])[0:19],
                               'user_id':x['user_id'],
                               'post_date':str(x['post_date'])[0:19],
                               'pick_up_date':x['pick_up_date'],
                               'pick_up_by_id':x['pick_up_by_id']
            })
            # | 'debug' >> beam.Map(debug_function)
            | 'writeToBQ' >> beam.io.Write(
                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','fact_answered_b'),
                      schema='id:STRING, post_date:TIMESTAMP, post_id:STRING, post_modified:TIMESTAMP, answer_time:TIMESTAMP, user_id:STRING, question_id:STRING, answer_by_id:STRING, answer_id:STRING, pick_up_date:TIMESTAMP, pick_up_by_id:STRING',
                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                      write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
            )
