import apache_beam as beam
import datetime
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient

def debug_function(pcollection_as_list):
    print (pcollection_as_list)

class GetSlugFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alodokter
        questions = db.core_posts
        questionList = []
        for topic in questions.find({'post_id':element['post_parent_id']}, no_cursor_timeout=True).limit(3):
            if topic['core_post_meta'] is not None:
                for i in topic['core_post_meta']:
                    if "custom_permalink" in i["meta_key"]:
                        slug = i['meta_value']
                    elif "custom_permalink" not in i["meta_key"]:
                        slug = topic['post_name']
            else:
                slug = topic['post_name']
            return { 'id':element['id'],
                     'post_name':slug,
                     'post_id':element['post_id'],
                     'post_title':element['post_title'],
                     'post_status':element['post_status'],
                     'post_date':element['post_date'],
                     'post_author':element['post_author'],
                     'post_parent_id':element['post_parent_id'],
                     'post_parent':element['post_parent']
                  }

# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.137.27/alodokter"
# connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.10/alodokter"
# connection_string = "mongodb://aloadmin:mutia.1975.dokter@35.187.240.157/admin"
# connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.27/alodokter"
connection_string = "mongodb://127.0.0.1:27017/pobpad"
tdelta = datetime.timedelta(hours=7)

def run():
   #  gcs_path = "gs://staging-plenary-justice-151004"
   #  dataflow_options = [
   #      "--project", "plenary-justice-151004",
   #      "--staging_location", ("%s/staging/" %gcs_path),
   #      "--temp_location", ("%s/temp" % gcs_path),
   #      "--region", "asia-east1",
   #      "--setup_file", "./setup.py"
   #  ]
   #  options = PipelineOptions(dataflow_options)
   #  gcloud_options = options.view_as(GoogleCloudOptions)
   #  pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
   #     "--project", "plenary-justice-151004",
   #     "--staging_location", ("%s/staging/" %gcs_path),
   #     "--temp_location", ("%s/temp" % gcs_path),
   #     "--region", "asia-east1",
   #     "--setup_file", "./setup.py"
   # ])

   options = PipelineOptions()
   google_cloud_options = options.view_as(GoogleCloudOptions)
   google_cloud_options.project = 'plenary-justice-151004'
   google_cloud_options.job_name = 'sourcemongo'
   google_cloud_options.staging_location = 'gs://staging-plenary-justice-151004/staging/'
   google_cloud_options.temp_location = 'gs://staging-plenary-justice-151004/temp'
   google_cloud_options.region = 'asia-east1'
   options.view_as(StandardOptions).runner = 'DirectRunner'
   options.view_as(SetupOptions).save_main_session = False
   requirements_file = "/home/grumpycat/flowarehouse/dataflow_pipeline/requirements.txt"
   options.view_as(SetupOptions).requirements_file = requirements_file

   p = beam.Pipeline(options=options)

    # with beam.Pipeline(options=options) as pipeline_reply:
   (p
             |'ReadReplyWeb' >> ReadFromMongo(connection_string, 'pobpad', 'core_posts',
                                    query={'post_type':'reply'},
                                    fields=['post_name', 'post_id', 'post_title', 'post_status', 'post_date', 'post_author', 'post_parent_id', 'post_parent'])
             |'FormatReply' >> beam.Map(lambda x:{'id':str(x['_id']), 'post_name':x['post_name'], 'post_id':str(x['post_id']), 'post_title':x['post_title'], 'post_status':x['post_status'], 'post_date':str(x['post_date']+tdelta)[0:19], 'post_author':str(x['post_author']), 'post_parent_id':x['post_parent_id'], 'post_parent':str(x['post_parent'])})
             # |'getslug' >> beam.ParDo(GetSlugFn())
             # |'DebugQuestions' >> beam.Map(debug_function)
             |'WriteRepliesToBQ' >> beam.io.Write(
               beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','pobpad_replies'),
               schema='id:STRING, post_id:STRING, post_name:STRING, post_title:STRING, post_status:STRING, post_date:DATETIME, post_author:STRING, post_parent_id:INTEGER, post_parent:STRING',
               create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
               write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
    )

   p.run().wait_until_finish()

if __name__ == '__main__':
    run()
