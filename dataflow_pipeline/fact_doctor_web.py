import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import datetime

def debug_function(pcollection_as_list):
    print (pcollection_as_list)

# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.240.137.27/alodokter"
connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.10/alodokter"



class GetNumberFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alodokter
        numQ = []
        post_date = datetime.datetime.strptime(element['post_date'], '%Y-%m-%d')
        number_of_questions = db.core_posts.find({'post_author':element['post_author'],
        'post_date':{"$gte":post_date, "$lte":post_date + datetime.timedelta(days = 1)}}).count()
        if number_of_questions != 0:
            numQ.append({'doctor_id':str(element['post_author']),
                        'post_date':element['post_date'],
                        'number_of_questions' : number_of_questions
                        })
        return numQ

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
    "--project", "plenary-justice-151004",
    "--staging_location", ("%s/staging/" %gcs_path),
    "--temp_location", ("%s/temp" % gcs_path),
    "--region", "asia-east1",
    "--setup_file", "./setup.py"
    ]
    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'

    #  pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
    #     "--project", "plenary-justice-151004",
    #     "--staging_location", ("%s/staging/" %gcs_path),
    #     "--temp_location", ("%s/temp" % gcs_path),
    #     "--region", "asia-east1",
    #     "--setup_file", "./setup.py"
    # ])

    with beam.Pipeline(options = options) as pipeline_fact_doctors:
        (pipeline_fact_doctors
             |'ReadFactDoctWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_posts',
                                      query={'post_type':'reply','post_date':{'$gte':datetime.datetime(2018,11,20)}}, fields=['post_date', 'post_author'])
             |'Format' >> beam.Map(lambda x:{'post_date':str(x['post_date'])[:10], 'post_author':x['post_author']})
             # |'GroupbyKey' >> beam.ParDo(SumQuestionFn())
             # |'Doc' >> beam.ParDo(GetDoctFn())
             |'number_of_questions' >> beam.ParDo(GetNumberFn())
             #|'Debug' >> beam.Map(debug_function)
             | 'WriteFactDoctorToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','fact_doctors'),
                     schema='post_date:DATE, number_of_questions:INTEGER, doctor_id:STRING',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )
