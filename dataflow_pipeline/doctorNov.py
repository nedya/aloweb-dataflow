import apache_beam as beam
import datetime
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import uuid, math


def debug_function(pcollection_as_list):
    print (pcollection_as_list)

tdelta = datetime.timedelta(hours=7)
#connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.10/alodokter"
connection_string = "mongodb://grumpycat:alo.1975.dokter@35.187.240.157/pobpad"
# connection_string = "mongodb://grumpycat:alo.1975.dokter@35.187.240.157/alodokter"

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py",
        "--num_workers", "7"
    ]

    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
# pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
#        "--project", "plenary-justice-151004",
#        "--staging_location", ("%s/staging/" %gcs_path),
#        "--temp_location", ("%s/temp" % gcs_path),
#        "--region", "asia-east1",
#        "--setup_file", "./setup.py"
#    ])

# with beam.Pipeline(runner = 'DirectRunner') as pipeline_doctor:
    with beam.Pipeline(options = options) as pipeline_doctor:
        (pipeline_doctor
            |'ReadDoctorsWeb' >> ReadFromMongo(connection_string, 'pobpad', 'core_users',
                                        query={'core_user_meta': {"$elemMatch": {"meta_key": "wp_capabilities", "meta_value": {"$regex": 'dokter'} } } },
                                        fields=['display_name', 'user_email', 'user_id', 'meta_key', 'meta_value', 'sign_in_count', 'last_sign_in_at', 'user_registered'])
            |'FormatDoctor' >> beam.Map(lambda x:{  'id':str(x['_id']),
                                            'doctor_id':int(x['user_id']),
                                            'email': x['user_email'],
                                            'display_name':x['display_name'],
                                            'sign_in_count': x['sign_in_count'],
                                            'last_sign_in_at': str( x.get('last_sign_in_at',""))[0:10],
                                            'register_date': str( x.get('user_registered',""))[0:10],
                                            'updated_at':str(datetime.datetime.now())[:19]})
            # |'DebugDoctor' >> beam.Map(debug_function)
            |'WriteDoctorToBQ' >> beam.io.Write(
              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','pobpad_doctors'),
              schema='id:STRING, doctor_id:INTEGER, email:STRING, register_date:STRING, display_name:STRING, sign_in_count:INTEGER, last_sign_in_at:STRING, updated_at:DATETIME',
              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
              write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
              )
