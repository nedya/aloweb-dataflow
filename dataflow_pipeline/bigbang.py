import apache_beam as beam
from datetime import datetime, timedelta
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from mongodbio import ReadFromMongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import uuid, math

def debug_function(pcollection_as_list):
    print (pcollection_as_list)

#dimensi question
def slugtopic(topic):
    if topic['post_date'] is None:
        post_date = None
    else:
        post_date = str(topic['post_date']+tdelta)[0:19]

    if topic['pickup_date'] is None:
        pickup_date = None
    else:
        pickup_date = str(topic['pickup_date']+tdelta)[0:19]

    if topic['core_post_meta'] is not None:
        for i in topic['core_post_meta']:
            if "custom_permalink" in i["meta_key"]:
                return {'post_id':topic['post_id'],
                        'post_title':topic['post_title'],
                        'post_status':topic['post_status'],
                        'post_date':post_date,
                        'pickup_date':pickup_date,
                        'slug':i['meta_value']}
            elif "custom_permalink" not in i["meta_key"]:
                return {'post_id':topic['post_id'],
                        'post_title':topic['post_title'],
                        'post_status':topic['post_status'],
                        'post_date':post_date,
                        'pickup_date':pickup_date,
                        'slug':topic['post_name']}

    else:
        return {'post_id':topic['post_id'],
                'post_title':topic['post_title'],
                'post_status':topic['post_status'],
                'post_date':post_date,
                'pickup_date':pickup_date,
                'slug':topic['post_name']}

def slugpost(post):
    if (post['post_title']):
        for i in post['core_post_meta']:
            if "custom_permalink" in i["meta_key"]:
                return {'id':post['id'], 'post_title':post['post_title'], 'slug':i['meta_value'], 'post_modified':str(post['post_modified'])}
            elif "custom_permalink" not in i["meta_key"]:
                return {'id':post['id'], 'post_title':post['post_title'], 'slug':post['post_name'],'post_modified':str(post['post_modified'])}

class GetTagFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alodokter
        data = []
        term_query = db.core_terms.find({'_id':element['term_id']})
        for term in term_query:
            data.append({'term_id':str(element['term_id']),
                         'tag':term['name']})
        return data

class DenormalizeMagFn(beam.DoFn):
    def process(self, element):
        core_term_relationships = element.get('core_term_relationships')
        magazines = []
        if core_term_relationships is not None and element.get('post_author') is not None and element.get('post_date') is not None:
            if (len(core_term_relationships) > 0):
                for term_taxonomy_ids in core_term_relationships:
                    for data in [term_taxonomy_ids]:
                        magazines.append({'post_id':str(element['_id']),
                                          'term_taxonomy_id':str(data['term_taxonomy_id']),
                                          'post_author':str(element['post_author']),
                                          'post_date':element['post_date']})
        return magazines

#fact_question
class DenormalizeQFn(beam.DoFn):
    def process(self, element):
        core_term_relationships = element.get('core_term_relationships',[])
        terms = []
        #if len(core_term_relationships) != 0:
        for core_term_relationship in core_term_relationships:
            for core_term in [core_term_relationship]:
                if core_term['term_taxonomy_id'] is not 'None':
                    term_taxonomies = MongoClient("mongodb://grumpycat:alo.1975.dokter@10.148.0.10/alodokter").alodokter.core_term_taxonomies
                    for term in term_taxonomies.find({'_id':core_term['term_taxonomy_id']}, no_cursor_timeout=True):
                        terms.append({'post_id':element['post_id'],
                                      'term_id':str(term['term_id']),
                                      'post_modified':element.get('post_modified', None)})
                # elif core_term['term_taxonomy_id'] is 'None':
                #     terms.append({'post_id':element['post_id'], 'term_id':None})
        return (terms)

class GetTermFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alodokter
        term_taxonomies = db.core_term_taxonomies
        term_list = []
        if (len(element['term_taxonomy_id']) > 0):
            for term in term_taxonomies.find({'_id':ObjectId(element['term_taxonomy_id'])}):
                term_list.append({'post_id': str(element['post_id']),
                                  'term_id': str(term['term_id']),
                                  'post_author':str(element['post_author']),
                                  'post_date':element['post_date']})
        return term_list

class GetQuestionFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alodokter
        questions = db.core_posts
        questionList = []
        for question in questions.find({'post_id':element['post_id']}, no_cursor_timeout=True).limit(3):
            if question.get('pickup_date',None) is not None:
                questionList.append({'post_id':element['post_id'],
                                     'question_id':str(question['_id']),
                                     'post_modified':str(element['post_modified']+tdelta),
                                     'term_id':element['term_id'],
                                     'user_id':str(question['post_author']),
                                     'post_date':str(question['post_date']+tdelta),
                                     'pickup_date':question['pickup_date']+tdelta,
                                     'pickup_by_id':str(question.get('pickup_by_id',"")),
                                     'post_hour_id':(question['post_date']+tdelta).hour,
                                     'post_minute_id':(question['post_date']+tdelta).minute,
                                     'post_date_id':(question['post_date']+tdelta).day,
                                     'post_month_id':(question['post_date']+tdelta).month,
                                     'post_year_id':(question['post_date']+tdelta).year})
            elif question.get('pickup_date',None) is None:
                questionList.append({'post_id':element['post_id'],
                                     'question_id':str(question['_id']),
                                     'post_id':element['post_id'],
                                     'post_modified':str(element['post_modified']+tdelta),
                                     'term_id':element['term_id'],
                                     'user_id':str(question['post_author']),
                                     'post_date':str(question['post_date']+tdelta),
                                     'pickup_date':None,
                                     'pickup_by_id':str(question.get('pickup_by_id',"")) ,
                                     'post_hour_id':(question['post_date']+tdelta).hour,
                                     'post_minute_id':(question['post_date']+tdelta).minute,
                                     'post_date_id':(question['post_date']+tdelta).day,
                                     'post_month_id':(question['post_date']+tdelta).month,
                                     'post_year_id':(question['post_date']+tdelta).year})
        return (questionList)

class GetAnswerFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alodokter
        answers = db.core_posts
        ansqueList = []
        for answer in answers.find({'post_parent_id':element['post_id']}, no_cursor_timeout=True):
            if element.get('pickup_date',None) is not None:
                ansqueList.append({'id':str(uuid.uuid4()),
                                   'term_id':element['term_id'],
                                   'question_id':element['question_id'],
                                   'post_id':element['post_id'],
                                   'post_modified':element['post_modified'],
                                   'answer_id':str(answer['_id']),
                                   'answeredby_id':str(answer['post_author']),
                                   'answer_time':str(answer['post_date']+tdelta),
                                   'answer_date_id':(answer['post_date']+tdelta).day,
                                   'answer_hour_id':(answer['post_date']+tdelta).hour,
                                   'answer_month_id':(answer['post_date']+tdelta).month,
                                   'answer_year_id':(answer['post_date']+tdelta).year,
                                   'answer_minute_id':(answer['post_date']+tdelta).minute,
                                   'user_id':str(element['user_id']),
                                   'post_date':str(element['post_date']),
                                   'pickup_date':str(element['pickup_date'])[0:19],
                                   'pick_date_id':element['pickup_date'].day,
                                   'pick_hour_id':element['pickup_date'].hour,
                                   'pick_month_id':element['pickup_date'].month,
                                   'pick_year_id':element['pickup_date'].year,
                                   'pick_minute_id':element['pickup_date'].minute,
                                   'pickup_by_id':str(element['pickup_by_id']) ,
                                   'post_hour_id':element['post_hour_id'],
                                   'post_minute_id':element['post_minute_id'],
                                   'post_date_id':element['post_date_id'],
                                   'post_month_id':element['post_month_id'],
                                   'post_year_id':element['post_year_id']})

            elif element.get('pickup_date',None) is None:
                ansqueList.append({'id':str(uuid.uuid4()),
                                   'term_id':element['term_id'],
                                   'question_id':element['question_id'],
                                   'post_id':element['post_id'],
                                   'post_modified':element['post_modified'],
                                   'answer_id':str(answer['_id']),
                                   'answeredby_id':str(answer['post_author']),
                                   'answer_time':str(answer['post_date']+tdelta),
                                   'answer_date_id':(answer['post_date']+tdelta).day,
                                   'answer_hour_id':(answer['post_date']+tdelta).hour,
                                   'answer_month_id':(answer['post_date']+tdelta).month,
                                   'answer_year_id':(answer['post_date']+tdelta).year,
                                   'answer_minute_id':(answer['post_date']+tdelta).minute,
                                   'user_id':str(element['user_id']),
                                   'post_date':str(element['post_date']),
                                   'pickup_date':None, 'pick_date_id':None,
                                   'pick_hour_id':None, 'pick_month_id':None,
                                   'pick_year_id':None, 'pick_minute_id':None,
                                   'pickup_by_id':str(element['pickup_by_id']) ,
                                   'post_hour_id':element['post_hour_id'],
                                   'post_minute_id':element['post_minute_id'],
                                   'post_date_id':element['post_date_id'],
                                   'post_month_id':element['post_month_id'],
                                   'post_year_id':element['post_year_id']})
        return (ansqueList)

#dimensi qa_filter
class GetAnsweredBy(beam.DoFn):
    def process(self, element):
        questions = []
        if (element.get('core_post_meta', None) is not None):
            for data in element ['core_post_meta']:
                #answered by QA
                if (data['meta_key'] == '_topic_answered_by_qa'):
                    if (data['meta_value']==True):
                        element['answered_by_qa'] = 1
                    elif (data['meta_value']==False):
                        element['answered_by_qa'] = 0
                    else: element['answered_by_qa'] = None

                #answered by doctor
                if (data['meta_key'] == '_bbp_topic_answered_by_doctor'):
                    element['answered_by_doctor'] = data['meta_value']

            #get question which only answered by doctor or qa
            if (element.get('answered_by_qa', None) is not None or element.get('answered_by_doctor', None) is not None):
                return [{'question_id':str(element['_id']),
                         'answered_by_qa':element.get('answered_by_qa', None),
                         'answered_by_doctor':int(element['answered_by_doctor']),
                         'post_modified':str(element['post_modified'])[0:19]}]

#dimensi user
class GetUserRegistered(beam.DoFn):
    def process(self, element):
        user = []
        client = MongoClient(connection_string)
        db = client.alodokter
        if element.get('user_registered') is not None:
                user.append({'id':str(element['_id']),
                            'user_email': element['user_email'],
                            'user_registered':str(element['user_registered'])[:19],
                            'display_name': element['display_name'],
                            'sign_in_count': element['sign_in_count'],
                            'updated_at':str(datetime.now())[:19]})
        else:
                user.append({'id':str(element['_id']),
                            'user_email': element['user_email'],
                            'user_registered':None,
                            'display_name': element['display_name'],
                            'sign_in_count': element['sign_in_count'],
                            'updated_at':str(datetime.now())[:19]})
        return user

#fact_doctors
class GetNumberFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alodokter
        numQ = []
        post_date = datetime.strptime(element['post_date'], '%Y-%m-%d')
        number_of_questions = db.core_posts.find({'post_author':element['post_author'],
        'post_date':{"$gte":post_date, "$lte":post_date + timedelta(days = 1)}}).count()
        if number_of_questions != 0:
            numQ.append({'doctor_id':str(element['post_author']),
                        'post_date':element['post_date'],
                        'number_of_questions' : number_of_questions
                        })
        return numQ

#fact_answered
class GetAnsweredFn(beam.DoFn):
    def process(self, element):
        client = MongoClient(connection_string)
        db = client.alodokter
        answers = db.core_posts
        ansqueList = []
        for answer in answers.find({'post_parent_id':element['post_id']}, no_cursor_timeout=True):
            if element.get('pickup_date',None) is not None:
                pick_up_date = element.get('pick_up_date')+tdelta
                pick_up_date = str(pick_up_date)[0:19]
            elif element.get('pickup_date',None) is None:
                pick_up_date = None
                ansqueList.append({'id':str(uuid.uuid4()),
                                   'question_id':str(element.get('_id', None)),
                                   'post_id':element['post_id'],
                                   'post_modified':element.get('post_modified', None)+tdelta,
                                   'answer_id':str(answer.get('_id', None)),
                                   'doctor_id':str(answer['post_author']),
                                   'answer_time':answer['post_date']+tdelta,
                                   'user_id':str(element.get('post_author', None)),
                                   'post_date':element.get('post_date', None)+tdelta,
                                   'pick_up_date':pick_up_date,
                                   'pick_up_by_id':str(element.get('pick_up_by_id'))
                                   })

            elif element.get('pickup_date',None) is None:
                ansqueList.append({'id':str(uuid.uuid4()),
                                   'question_id':str(element.get('_id', None)),
                                   'post_id':element['post_id'],
                                   'post_modified':element.get('post_modified', None)+tdelta,
                                   'answer_id':str(answer.get('_id', None)),
                                   'doctor_id':str(answer['post_author']),
                                   'answer_time':answer['post_date']+tdelta,
                                   'user_id':str(element.get('post_author', None)),
                                   'post_date':element.get('post_date', None)+tdelta,
                                   'pick_up_date':pick_up_date,
                                   'pick_up_by_id':None
                                   })
        return (ansqueList)

tdelta = timedelta(hours=7)
connection_string = "mongodb://grumpycat:alo.1975.dokter@10.148.0.10/alodokter"

def run():
    gcs_path = "gs://staging-plenary-justice-151004"
    dataflow_options = [
        "--project", "plenary-justice-151004",
        "--staging_location", ("%s/staging/" %gcs_path),
        "--temp_location", ("%s/temp" % gcs_path),
        "--region", "asia-east1",
        "--setup_file", "./setup.py",
        "--num_workers", "7"
    ]

    options = PipelineOptions(dataflow_options)
    gcloud_options = options.view_as(GoogleCloudOptions)
    options.view_as(StandardOptions).runner = 'dataflow'
# pipeline = beam.Pipeline (runner="DataflowRunner", argv=[
#        "--project", "plenary-justice-151004",
#        "--staging_location", ("%s/staging/" %gcs_path),
#        "--temp_location", ("%s/temp" % gcs_path),
#        "--region", "asia-east1",
#        "--setup_file", "./setup.py"
#    ])

# def run():
#    options = PipelineOptions()
#    google_cloud_options = options.view_as(GoogleCloudOptions)
#    google_cloud_options.project = 'plenary-justice-151004'
#    google_cloud_options.job_name = 'sourcemongo'
#    google_cloud_options.staging_location = 'gs://staging-plenary-justice-151004/staging/'
#    google_cloud_options.temp_location = 'gs://staging-plenary-justice-151004/temp'
#    google_cloud_options.region = 'asia-east1'
#    options.view_as(StandardOptions).runner = 'DirectRunner'
#    options.view_as(SetupOptions).save_main_session = True

   # pipeline = beam.Pipeline(options=options)
    with beam.Pipeline(options = options) as pipeline_tags:
        (pipeline_tags
             | 'Read_tags' >> ReadFromMongo(connection_string, 'alodokter', 'core_terms',
                                    query={}, fields=['_id', 'name'])
             | 'FormatData' >> beam.Map (lambda x:{'term_id':str(x['_id']),
                                                   'tag':x['name']})
             #| 'Debug' >> beam.Map(debug_function)
             | 'writeterm_magazineToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','tags'),
                   schema='term_id:STRING, tag:STRING',
                   create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                   write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

    with beam.Pipeline(options = options) as pipeline_terms_questions:
        (pipeline_terms_questions
            |'ReadTermTopicWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_term_taxonomies', query={'taxonomy':'topic-tag'}, fields=['term_id'])
            |'GetTagTopic' >> beam.ParDo(GetTagFn())
    #     #     | 'DebugTermsQuestions' >> beam.Map(debug_function)
            |'WriteTermsQuestionsToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','terms_topic'),
              schema='term_id:STRING, tag:STRING',
              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
              write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

    # with beam.Pipeline(options = options) as pipeline_editor:
#         (pipeline_editor
#             |'ReadEditorWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_users',
#                                             query={'core_user_meta': {"$elemMatch": {"meta_key": "wp_capabilities", "meta_value": {"$regex": 'editor'} } }}, fields=['display_name'])
#             |'FormatDataEditor' >> beam.Map (lambda x:{'id':str(x['_id']), 'name':x['display_name'],'updated_at':str(datetime.now())[:19]})
# #             |'DebugEditor' >> beam.Map(debug_function)
#             |'WriteEditorToBQ' >> beam.io.Write(
#               beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','editor'),
#               schema='id:STRING, name:STRING, updated_at:DATETIME',
#               create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
#               write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
#         )

    with beam.Pipeline(options = options) as pipeline_doctor:
        (pipeline_doctor
            |'ReadDoctorsWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_users',
                                        query={'core_user_meta': {"$elemMatch": {"meta_key": "wp_capabilities", "meta_value": {"$regex": 'dokter'} } } },
                                        fields=['display_name', 'user_email', 'user_id', 'meta_key', 'meta_value', 'sign_in_count', 'last_sign_in_at', 'user_registered'])
            |'FormatDoctor' >> beam.Map(lambda x:{  'id':str(x['_id']),
                                            'doctor_id':int(x['user_id']),
                                            'email': x['user_email'],
                                            'display_name':x['display_name'],
                                            'sign_in_count': x['sign_in_count'],
                                            'last_sign_in_at': str( x.get('last_sign_in_at',""))[0:10],
                                            'register_date': str( x.get('user_registered',""))[0:10],
                                            'updated_at':str(datetime.now())[:19]})
# # #              |'DebugDoctor' >> beam.Map(debug_function)
            |'WriteDoctorToBQ' >> beam.io.Write(
              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','doctors'),
              schema='id:STRING, doctor_id:INTEGER, email:STRING, register_date:STRING, display_name:STRING, sign_in_count:INTEGER, last_sign_in_at:STRING, updated_at:DATETIME',
              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
              write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

    with beam.Pipeline(options = options) as pipeline_author:
        (pipeline_author
            |'ReadAuthorsWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_users', query={'core_user_meta': {"$elemMatch": {"meta_key": "wp_capabilities", "meta_value": {"$regex": 'author'} } }}, fields=['display_name'])
            |'FormatAuthor' >> beam.Map (lambda x:{'id':str(x['_id']), 'name':x['display_name'],'updated_at':str(datetime.now())[:19]})
# #             |'DebugAuthor' >> beam.Map(debug_function)
            |'WriteAuthorToBQ' >> beam.io.Write(
              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','authors'),
              schema='id:STRING, name:STRING, updated_at:DATETIME',
              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
              write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

    with beam.Pipeline(options = options) as pipeline_articles:
        (pipeline_articles
            |'ReadArticlesWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_posts',
                                query={'post_modified':{'$gte':datetime.today() - timedelta(days = 1) - timedelta(hours=7)}, 'post_status': 'publish', 'post_type': 'post'},
                                fields=['post_name','post_title','core_post_meta', 'post_modified'])
            |'FormatArticles' >> beam.Map(lambda x:{'id':str(x['_id']),
                                                    'post_name':x['post_name'],
                                                    'post_title':x['post_title'],
                                                    'core_post_meta':x['core_post_meta'],
                                                    'post_modified':x['post_modified']})
            |'SlugPost' >> beam.Map(slugpost)
#             # |'DebugArticles' >> beam.Map(debug_function)
            |'WriteArticlesToBQ' >> beam.io.Write(
              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','articles'),
              schema='id:STRING, slug:STRING, post_title:STRING, post_modified:DATETIME',
              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
              write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )

    with beam.Pipeline(options = options) as pipeline_popularity:
         (pipeline_popularity
              |'ReadPopularityWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_popular_data_posts', query={}, fields=['post_id', 'pageviews'])
              |'FormatPopularity' >> beam.Map (lambda x:{ 'article_id':str(x['post_id']), 'total_pageviews':x['pageviews']})
#  #              | 'DebugPopularity' >> beam.Map(debug_function)
              |'WritePopularityToBQ' >> beam.io.Write(
                beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','popularities'),
                schema='article_id:STRING, total_pageviews:INTEGER',
                create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
         )

    with beam.Pipeline(options = options) as pipeline_questions:
        (pipeline_questions
             |'ReadQuestionsWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_posts',
                                    query={'post_date':{'$gte':datetime.today() - timedelta(days = 1) - timedelta(hours=7)}, 'post_type':'topic'},
                                    fields=['core_post_meta', 'post_name', 'post_id', 'post_title', 'post_status', 'post_date', 'pickup_date'])
             |'FormatQuestions' >> beam.Map(lambda x:{'core_post_meta':x.get('core_post_meta'),
                                                      'post_name':x['post_name'],
                                                      'post_id':str(x['_id']),
                                                      'post_title':x['post_title'],
                                                      'post_status':x['post_status'],
                                                      'post_date':x.get('post_date',None),
                                                      'pickup_date':x.get('pickup_date', None)})
             |'SlugTopic' >> beam.Map(slugtopic)
# # #              |'DebugQuestions' >> beam.Map(debug_function)
             |'WriteQuestionsToBQ' >> beam.io.Write(
               beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','questions'),
               schema='post_id:STRING, slug:STRING, post_title:STRING, post_status:STRING, post_date:DATETIME, pickup_date:DATETIME',
               create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
               write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )


    with beam.Pipeline(options = options) as pipeline_question_per_day:
        (pipeline_question_per_day
             | 'Readquestion_per_dayWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_posts',
                                      query={'post_date':{'$gte':datetime.today() - timedelta(days = 1) - timedelta(hours=7)}}, fields=['post_date'])
             | 'Format' >> beam.Map(lambda x:(str(x['post_date'])[0:10], x['_id'] ))
             | 'Count' >> beam.GroupByKey()
             | 'len' >> beam.Map(lambda (x,y):{'post_date':x, 'number_of_questions':len(list(y))})
#             # |'Debugy' >> beam.Map(debug_function)
             | 'WritePageViewToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','question_per_day'),
                     schema='post_date:DATETIME, number_of_questions:INTEGER',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
             )

    with beam.Pipeline(options = options) as pipeline_pageview_per_day:
        (pipeline_pageview_per_day
            |'ReadPageviewWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_popular_summary_posts', query={}, fields=['post_id', 'view_date', 'pageviews'])
            |'FormatPageview' >> beam.Map (lambda x:{'article_id':str(x['post_id']), 'view_date':str(x['view_date'])[0:10], 'pageviews_per_day':x['pageviews']})
# #             |'DebugPageView' >> beam.Map(debug_function)
            |'WritePageViewToBQ' >> beam.io.Write(
              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','pageviews_per_day'),
              schema='article_id:STRING, view_date:DATE, pageviews_per_day:INTEGER',
              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
              write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
        )

    with beam.Pipeline(options = options) as pipeline_fact_magazine:
        (pipeline_fact_magazine
            |'ReadMagazineWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_posts',
                                query={'post_date':{'$gte':datetime.today() - timedelta(days = 1) - timedelta(hours=7)}, 'post_type':'post','post_status' : 'publish'}, fields=['post_author', 'post_date', 'core_term_relationships.term_taxonomy_id'])
            |'Denormalize' >> beam.ParDo(DenormalizeMagFn())
            |'GetTerm' >> beam.ParDo(GetTermFn())
            |'FormatData' >> beam.Map(lambda x:{'id':str(uuid.uuid4()),
                                               'article_id':(x['post_id']),
                                               'term_id': x['term_id'],
                                               'author_id':x['post_author'],
                                               'post_date':str(x['post_date']),
                                               'day_id': x['post_date'].isoweekday(),
                                               'date_id':x['post_date'].day,
                                               'month_id':x['post_date'].month,
                                               'year_id':x['post_date'].year,
                                               'hour_id':x['post_date'].hour,
                                               'minute_id':x['post_date'].minute,
                                               'quarter_id':int(math.ceil(x['post_date'].month/3.))})
 # #              |'DebugFactMagazine' >> beam.ParDo (debug_function)
            |'WriteFactMagazineToBQ' >> beam.io.Write(
              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','fact_magazine'),
              schema='id:STRING, article_id:STRING, author_id:STRING, post_date:DATETIME, day_id:INTEGER, date_id:INTEGER, month_id:INTEGER, quarter_id:INTEGER, year_id:INTEGER, hour_id:INTEGER, minute_id:INTEGER, term_id:STRING',
              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
              write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )

    with beam.Pipeline(options = options) as pipeline_fact_question:
        (pipeline_fact_question
            |'ReadFactQuestionWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_posts',
                                    query={'post_modified':{'$gte':datetime.today() - timedelta(days = 1) - timedelta(hours=7)}, 'post_type':'topic', 'core_term_relationships':{'$exists':True}},
                                    fields=['post_id', 'core_term_relationships', 'post_modified'])
            |'GetTag' >> beam.ParDo(DenormalizeQFn())
            |'ReadQuestions' >> beam.ParDo(GetQuestionFn())
            |'ReadAnswers' >> beam.ParDo(GetAnswerFn())
    #         #|'DebugFactQuestions' >> beam.Map(debug_function)
            |'WriteFactQuestionsToBQ1' >> beam.io.Write(
              beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','fact_questions'),
              schema='id:STRING, question_id:STRING, term_id:STRING, answer_id:STRING, user_id:STRING, pickup_by_id:STRING, post_modified:DATETIME, post_id:INTEGER, post_date:DATETIME, post_date_id:INTEGER, post_month_id:INTEGER, post_year_id:INTEGER, post_hour_id:INTEGER, post_minute_id:INTEGER, pickup_date:DATETIME, pick_date_id:INTEGER, pick_month_id:INTEGER, pick_year_id:INTEGER, pick_hour_id:INTEGER, pick_minute_id:INTEGER, answer_time:DATETIME, answer_date_id:INTEGER, answer_month_id:INTEGER, answer_year_id:INTEGER, answer_hour_id:INTEGER, answer_minute_id:INTEGER, answeredby_id:STRING',
              create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
              write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
       )

    with beam.Pipeline(options = options) as pipeline_fact_doctors:
        (pipeline_fact_doctors
             |'ReadFactDoctWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_posts',
                                      query={'post_type':'reply','post_date':{'$gte':datetime.today() - timedelta(days = 1) - timedelta(hours=7)}}, fields=['post_date', 'post_author'])
             |'Format' >> beam.Map(lambda x:{'post_date':str(x['post_date'])[:10], 'post_author':x['post_author']})
             |'number_of_questions' >> beam.ParDo(GetNumberFn())
    #         # |'Debug' >> beam.Map(debug_function)
             | 'WriteFactDoctorToBQ' >> beam.io.Write(
                     beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','fact_doctors'),
                     schema='post_date:DATE, number_of_questions:INTEGER, doctor_id:STRING',
                     create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )

    with beam.Pipeline(options=options) as pipeline_reply:
        (pipeline_reply
             |'ReadReplyWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_posts',
                                    query={'post_date':{'$gte':datetime.today() - timedelta(days = 1) - timedelta(hours=7)}, 'post_type':'reply'},
                                    fields=['post_name', 'post_id', 'post_title', 'post_status', 'post_date', 'post_author', 'post_parent_id', 'post_parent' ])
             |'FormatReply' >> beam.Map(lambda x:{'id':str(x['_id']), 'post_name':x['post_name'], 'post_id':str(x['_id']), 'post_title':x['post_title'], 'post_status':x['post_status'], 'post_date':str(x['post_date']+tdelta)[0:19], 'post_author':str(x['post_author']), 'post_parent_id':x['post_parent_id'], 'post_parent':str(x['post_parent'])})
             # |'DebugQuestions' >> beam.Map(debug_function)
             |'WriteRepliesToBQ' >> beam.io.Write(
               beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','replies'),
               schema='id:STRING, post_id:STRING, post_name:STRING, post_title:STRING, post_status:STRING, post_date:DATETIME, post_author:STRING, post_parent_id:INTEGER, post_parent:STRING',
               create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
               write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
        )

    with beam.Pipeline(options = options) as pipeline_users:
        (pipeline_users
           |'ReadUsersWeb' >> ReadFromMongo(connection_string, 'alodokter', 'core_users',
                            query={'core_user_meta': {"$elemMatch": {"meta_key": "wp_capabilities", "meta_value": {"$regex": 'bbp_participant' } } } },
                            fields=['display_name','user_email', 'user_registered', 'meta_key', 'meta_value', 'sign_in_count', 'last_sign_in_at'])
           |'user_registered' >> beam.ParDo(GetUserRegistered())
    #        # |'Debug' >> beam.Map(debug_function)
           |'WriteURToBQ' >> beam.io.Write(beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','users'),
                            schema='id:STRING, user_email:STRING, user_registered:DATETIME, display_name:STRING, sign_in_count:INTEGER, updated_at:DATETIME',
                            create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                            write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))
         )

    with beam.Pipeline(options=options) as pipeline_fact_answered:
        (pipeline_fact_answered
            | 'read_factquestion' >> ReadFromMongo(connection_string, 'alodokter', 'core_posts',
                        query={'post_type':'topic', 'post_date':{'$gte':datetime.today() - timedelta(days = 1) - timedelta(hours=7)}},
                        fields=['post_id', 'post_modified', 'post_author', 'post_date', 'pick_up_date', 'pick_up_by_id'])
            | 'read_answers' >> beam.ParDo(GetAnsweredFn())
            | 'FormatData' >> beam.Map(lambda x:{'id':x['id'],
                               'question_id':x['question_id'],
                               'post_id':str(x['post_id']),
                               'post_modified':str(x['post_modified'])[0:19],
                               'answer_id':x['answer_id'],
                               'answer_by_id':x['doctor_id'],
                               'answer_time':str(x['answer_time'])[0:19],
                               'user_id':x['user_id'],
                               'post_date':str(x['post_date'])[0:19],
                               'pick_up_date':x['pick_up_date'],
                               'pick_up_by_id':x['pick_up_by_id']
            })
            # | 'debug' >> beam.Map(debug_function)
            | 'writeToBQ' >> beam.io.Write(
                      beam.io.BigQuerySink('{}:{}.{}'.format('plenary-justice-151004','alowarehouse_alodokterweb','fact_answered'),
                      schema='id:STRING, post_date:TIMESTAMP, post_id:STRING, post_modified:TIMESTAMP, answer_time:TIMESTAMP, user_id:STRING, question_id:STRING, answer_by_id:STRING, answer_id:STRING, pick_up_date:TIMESTAMP, pick_up_by_id:STRING',
                      create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                      write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))
            )
